﻿const { createClient } = require('@commercetools/sdk-client')
const { createAuthMiddlewareForClientCredentialsFlow } = require('@commercetools/sdk-middleware-auth')
const { createHttpMiddleware } = require('@commercetools/sdk-middleware-http')
const fetch = require('node-fetch')

const projectKey = 'aicpastore_2019'

const authMiddleware = createAuthMiddlewareForClientCredentialsFlow({
    host: 'https://auth.commercetools.co',
    projectKey,
    credentials: {
        clientId: 'cQKoR_qTWVoXxRhMkvyvp6LJ',
        clientSecret: 'djB3QEi3ZnVO85hht6mo-Zz2J-RlzGmc',
    },
    scopes: ['manage_subscriptions:aicpastore_2019 manage_orders:aicpastore_2019 manage_shopping_lists:aicpastore_2019 manage_customers:aicpastore_2019 manage_order_edits:aicpastore_2019 manage_products:aicpastore_2019'],
    fetch,
})
const httpMiddleware = createHttpMiddleware({
    host: 'https://api.commercetools.co',
    fetch,
})
const client = createClient({
    middlewares: [authMiddleware, httpMiddleware],
})

module.exports = {
    ProjectKey: projectKey,
    AuthMiddleWare: authMiddleware,
    HttpMiddleWare: httpMiddleware,
    Client: client
}