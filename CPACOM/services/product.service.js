﻿"use strict";

var ecomtools = require('./ecomtools');

module.exports = {
    name: "product",

	/**
	 * Service settings
	 */
    settings: {

    },

	/**
	 * Service metadata
	 */
    metadata: {

    },

	/**
	 * Service dependencies
	 */
    //dependencies: [],	

	/**
	 * Actions
	 */
    actions: {

		/**
		 * Say a 'Hello'
		 *
		 * @returns
		 */
        getAll() {
            var getrequest = {
                uri: '/' + ecomtools.ProjectKey + '/products',
                method: 'GET'
            }

            return ecomtools.Client.execute(getrequest);
        },

        /**
         * 
         * 
         */
        getById: {
            params: {
                id: "string"
            },
            handler(ctx) {
                var getrequest = {
                    uri: '/' + ecomtools.ProjectKey + '/products/' + ctx.params.id,
                    method: 'GET'
                }

                return ecomtools.Client.execute(getrequest);
            }
        }
    },

	/**
	 * Events
	 */
    events: {

    },

	/**
	 * Methods
	 */
    methods: {

    },

	/**
	 * Service created lifecycle event handler
	 */
    created() {

    },

	/**
	 * Service started lifecycle event handler
	 */
    started() {

    },

	/**
	 * Service stopped lifecycle event handler
	 */
    stopped() {

    }
};